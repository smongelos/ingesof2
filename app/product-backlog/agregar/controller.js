app.controller('AgregarBacklogCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
      
        
        $scope.datos = {};
        $scope.datos.descripcion;
        $scope.datos.idHistoriasUsuarios;
        parametros = 'historias-usuarios';

        blockUI();
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.historia = response.data.lista;
                    console.log($scope.historia);

                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });
       


        $scope.insertar = function () {
            tipos = 'backlog';
            parametros = {
                "descripcion": $scope.datos.descripcion,
                "idHistoriasUsuarios": $scope.datos.idHistoriasUsuarios
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        $state.go('app.product-backlog');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }

        $scope.limpiar = function () {
            $scope.datos.descripcion = "";
            $scope.datos.idHistoriasUsuarios = "";
        }


        $scope.cancelar = function () {
            $state.go('app.product-backlog');
        };
    }]);