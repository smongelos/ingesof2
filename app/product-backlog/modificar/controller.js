app.controller('ModificarBacklogCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.selected = {};
        $scope.selected.productBacklog = $localStorage.productBacklog;
        console.log($scope.selected.productBacklog)
        parametros = 'historias-usuarios';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioBack').modal('show');
        }

        blockUI();
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.historia = response.data.lista;
                    console.log($scope.historia);

                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });


        $scope.modificar = function () {
            tipos = 'backlog',
            parametros = {
                "idBacklog": $scope.selected.productBacklog.id_backlog,
                "descripcion": $scope.selected.productBacklog.backlog,
                "idHistoriasUsuarios":$scope.selected.productBacklog.idHistoriasUsuarios
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $state.go('app.product-backlog');
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                        unBlockUI();
                    });
        }

        $scope.cancelar = function () {
            $state.go('app.product-backlog');
        };
    }]);