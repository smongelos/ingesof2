app.controller('ProductBacklogCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        $scope.productBacklog;


        $scope.listaProductBacklog = function () {
            blockUI();
            MainFactory.listarBacklog()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.productBacklog = response.data.lista;
                        console.log($scope.proyectoAsignado);
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                                counts: [],
                                total: $scope.productBacklog.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.productBacklog, params.orderBy()) : $scope.productBacklog;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.listaProductBacklog();

        $scope.verMAs = function () {
            $state.go('app.historias-usuario');
        };
        $scope.modificar = function (productBacklog) {
            $localStorage.productBacklog = productBacklog;
            $state.go('app.modificar-backlog');
        };

        $scope.eliminar = function (productBacklog) {
            $localStorage.productBacklog = productBacklog;
            parametros = 'backlog';
            nroId = $localStorage.productBacklog.id_backlog;
            console.log($localStorage.productBacklog.id_backlog)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.info('Se elimino correctamente');
                        $scope.listaProductBacklog();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        };


        $scope.agregar = function () {
            $state.go('app.agregar-backlog');
        };

    }]);