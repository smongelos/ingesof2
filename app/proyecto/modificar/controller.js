app.controller('ModificarProyectoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        console.log($scope.selected.modificarDatos);
        $scope.selected.modificarDatos.fechaInicio = new Date();
        $scope.selected.modificarDatos.fechaFin = new Date();

        tipos = 'proyectos';
        parametros = 'proyectos'
        
        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#modificarPoryect').modal('show');
        }


        $scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }
        $scope.estados = function () {
            blockUI();
            parametros = 'estados'
            $scope.estado = [];
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.estado = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.estados();

        $scope.modificarProyecto = function () {

            var parametros = {
                "idClientes": 1,
                "nombre": $scope.selected.modificarDatos.nombre,
                "idProyectos": $scope.selected.modificarDatos.idProyectos,
                "fechaInicio": $scope.selected.modificarDatos.fechaInicio,
                "idEstados": $scope.selected.modificarDatos.idEstados,
                "fechaFin": $scope.selected.modificarDatos.fechaFin,
                "alias": $scope.selected.modificarDatos.alias
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $state.go('app.proyecto');
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                        unBlockUI();
                    });



        }


        $scope.cancelar = function () {
            $state.go('app.proyecto');
        };
    }]);