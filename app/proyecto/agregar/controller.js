app.controller('AgregarProyectoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
       

        tipos = 'proyectos';
        $scope.datos = {};
        $scope.datos.nombre;
        $scope.datos.fechaInicio = new Date();
        $scope.datos.idEstados;
        $scope.datos.fechaFin = new Date();
        $scope.datos.alias;

        $scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }

        $scope.estados = function () {
            blockUI();
            parametros = 'estados'
            $scope.estado = [];
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.estado = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.estados();

        $scope.insertar = function () {
            parametros = {
                "idClientes": 1,
                "nombre": $scope.datos.nombre,
                "fechaInicio":$scope.datos.fechaInicio,
                "idEstados": $scope.datos.idEstados,
                "fechaFin": $scope.datos.fechaFin,
                "alias": $scope.datos.alias
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        $state.go('app.proyecto');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }

        $scope.limpiar = function () {
            $scope.datos.nombre = "";
            $scope.datos.fechaInicio = "";
            $scope.datos.idEstados = "";
            $scope.datos.fechaFin = "";
            $scope.datos.alias = "";
        }

        $scope.cancelar = function () {
            $state.go('app.proyecto');
        };
    }]);