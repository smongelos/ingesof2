app.controller('ProyectoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        

        $scope.proyecto;
        parametros = 'proyectos';

        $scope.listarProyectos = function () {
            blockUI();
            MainFactory.listarProyectosVista()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.proyecto = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                                counts: [],
                                total: $scope.proyecto.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.proyecto, params.orderBy()) : $scope.proyecto;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }


        $scope.listarProyectos();

        $scope.modificar = function (proyecto) {
            $localStorage.modificarDatos = proyecto;
            console.log(proyecto)
            $state.go('app.modificar-proyecto');
        };

        $scope.eliminar = function (proyecto) {
            $localStorage.eliminarDatos = proyecto;
            parametros = 'proyectos';
            nroId = $localStorage.eliminarDatos.idProyectos;
            console.log($localStorage.eliminarDatos.idProyectos)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.info('Se elimino el Proyecto');
                        $scope.listarProyectos();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        };

        $scope.users = function (proyecto) {
            $localStorage.asignarProyecto = proyecto;
            $state.go('app.proyecto-asignado');
        };

        $scope.verMAs = function () {
            $state.go('app.product-backlog');
        };

        $scope.agregar = function () {
            $state.go('app.agregar-proyecto');
        };

    }]);