app.controller('AgregarHistoryUsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
        $scope.datos.idProyectos;
        $scope.datos.descripcion;

        $scope.insertar = function () {
            tipos = 'historias-usuarios';
            var parametros = {
                "idProyectos": $scope.datos.idProyectos,
                "estado": "En curso",
                "descripcion": $scope.datos.descripcion
            }
            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        $state.go('app.historias-usuario');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }

        parametros = 'proyectos'
        blockUI();
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.proyecto = response.data.lista;
                    console.log($scope.proyecto);

                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });

        $scope.limpiar = function () {
            $scope.datos.idProyectos = "";
            $scope.datos.nombre = "";
        }

        $scope.cancelar = function () {
            $state.go('app.historias-usuario');
        };
    }]);