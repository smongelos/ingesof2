app.controller('HistoriaUsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.historias;
        //parametros = 'historias-usuarios';
        // var parametro = {};
        $scope.listarHistorias = function () {
            blockUI();
            MainFactory.listarHistoriasVistas()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.historias = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                                counts: [],
                                total: $scope.historias.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.historias, params.orderBy()) : $scope.historias;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.listarHistorias();

        $scope.modificar = function (historias) {
            $localStorage.modificarDatos = historias;
            $state.go('app.modificar-history-usuario');
        };
        $scope.agregar = function () {
            $state.go('app.agregar-history-usuario');
        };

        $scope.eliminar = function (historias) {
            $localStorage.historias = historias;
            parametros = 'historias-usuarios';
            nroId = $localStorage.historias.id_historias_usuarios;
            console.log($localStorage.historias.id_historias_usuarios)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.info('Se elimino correctamente');
                        $scope.listarHistorias();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        };

    }]);