app.controller('ModificarHistoryUsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        console.log($scope.selected.modificarDatos)

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }

            $('#modificarHistoria').modal('show');
        }

        $scope.modificarHistoria = function () {

            tipos = 'historias-usuarios';
            var parametros = {
                "idProyectos": $scope.selected.modificarDatos.idProyectos,
                "estado": "En curso",
                "descripcion": $scope.selected.modificarDatos.descripcion,
                "idHistoriasUsuarios": $scope.selected.modificarDatos.id_historias_usuarios
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        console.log('puto')
                        /*$localStorage.socio = response.data.dato;
                        $localStorage.logueado = true;*/
                        //toastr.info('Datos Actualizados');
                        $state.go('app.historias-usuario');
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                        unBlockUI();
                    });



        }

        parametros = 'proyectos'
        blockUI();
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.proyecto = response.data.lista;
                    console.log($scope.proyecto);

                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });


        $scope.cancelar = function () {
            $state.go('app.historias-usuario');
        };
    }]);