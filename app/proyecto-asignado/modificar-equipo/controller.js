app.controller('ModificarEquipoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.usuario;
        parametros = 'usuarios';
        $scope.selected = {};
        $scope.selected.proyectoAsignado = $localStorage.proyectoAsignado;
        console.log($scope.selected.proyectoAsignado);

        // var parametro = {};

        tipos = 'usuarios';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioPA').modal('show');
        }

        $scope.modificar = function () {
            tipos = 'proyectos-asignados';
            parametros = {
                "idProyectosAsignados": $scope.selected.proyectoAsignado.id_proyectosAsignados,
                "idProyectos": $scope.selected.proyectoAsignado.idProyectos,
                "idUsuarios": $scope.selected.proyectoAsignado.idUsuarios,
                "idRoles": $scope.selected.proyectoAsignado.idRoles
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {

                        $state.go('app.proyecto-asignado');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }


        blockUI();
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.usuario = response.data.lista;
                    console.log($scope.usuario)

                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });


        blockUI();
        MainFactory.listarRoles()
            .then(function (response) {
                if (response.status === 200) {
                    $scope.roles = response.data.lista;
                    console.log($scope.roles);
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });


        $scope.cancelar = function () {
            $state.go('app.proyecto-asignado');
        };

    }]);