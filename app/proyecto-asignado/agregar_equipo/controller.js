app.controller('AgregarEquipoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.usuario;
        parametros = 'usuarios';
        $scope.selected = {};
        $scope.selected.proyectoAsignado = $localStorage.proyectoAsignado;
        console.log($scope.selected.proyectoAsignado);

        // var parametro = {};

        tipos = 'usuarios';
        $scope.datos = {};
        $scope.datos.id_proyectos;
        $scope.datos.idUsuarios;
        $scope.datos.idRoles;

        $scope.insertar = function () {
            tipos = 'proyectos-asignados'
            parametros = {
                "idProyectos": $scope.selected.proyectoAsignado.idProyectos,
                "idUsuarios": $scope.datos.idUsuarios,
                "idRoles": $scope.datos.idRoles
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;

                        $state.go('app.proyecto-asignado');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }


        blockUI();
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.usuario = response.data.lista;
                    console.log($scope.usuario)

                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });


        blockUI();
        MainFactory.listarRoles()
            .then(function (response) {
                if (response.status === 200) {
                    $scope.roles = response.data.lista;
                    console.log($scope.roles);
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });


        $scope.agregar = function () {
            $state.go('app.agregar-equipo');
        };

        $scope.cancelar = function () {
            $state.go('app.proyecto-asignado');
        };

    }]);