app.controller('ProyectoAsignadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.proyectoAsignado;
        parametros = 'proyectos-asignados';
        $scope.selected = {};
        $scope.selected.asignarProyecto = $localStorage.asignarProyecto;
        $scope.listaFiltro = [];
        console.log($scope.selected.asignarProyecto);
        
        $scope.listaProyectoAsignados = function () {
            blockUI();
            MainFactory.listarProyectos()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.proyectoAsignado = response.data.lista;
                       
                        for (var i in $scope.proyectoAsignado) {
                            if ($scope.proyectoAsignado[i].idProyectos == $scope.selected.asignarProyecto.idProyectos) {
                                $scope.listaFiltro.push($scope.proyectoAsignado[i]);
                            }
                        }
                        if ($scope.listaFiltro.length>0){
                            console.log($scope.listaFiltro);
                                $scope.tableParams = new NgTableParams({
                                    page: 1,
                                    count: 10,
                                }, {
                                        counts: [],
                                        total: $scope.listaFiltro.length,
                                        getData: function (params) {
                                            var orderedData = params.sorting() ? $filter('orderBy')($scope.listaFiltro, params.orderBy()) : $scope.proyectoAsignado;
                                            return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                        }
                                });
                        }
                                



                          /*else {
                               // toastr.info('No hay equipo relacionados a este proyectos', 'Atención');
                           }*/
                                       

                    } else {
                        toastr.info(response.data.mensaje);
                    }

                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.listaProyectoAsignados();


        $scope.agregar = function (proyectoAsignado) {
            $localStorage.proyectoAsignado = proyectoAsignado;
            $state.go('app.agregar-equipo');
        };


        $scope.modificar = function (proyectoAsignado) {
            $localStorage.proyectoAsignado = proyectoAsignado;
            $state.go('app.modificar-equipo');
        };

        $scope.eliminar = function (proyectoAsignado) {
            $localStorage.proyectoAsignado = proyectoAsignado;
            parametros = 'proyectos-asignados';
            nroId = $localStorage.proyectoAsignado.id_proyectosAsignados;
            console.log($localStorage.proyectoAsignado.id_proyectosAsignados)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.info('Se elimino correctamente');
                        $scope.listaProyectoAsignados();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        };

        $scope.cancelar = function () {
            $state.go('app.proyecto-asignado');
        };

    }]);