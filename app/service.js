app.factory('MainFactory', ['$http', function ($http) {
    return {
        listar: function (parametros) {
            return $http.get(`is2/rest/${parametros}/listar`);
        },

        modificarDatosActuales: function (parametros, tipos) {
            return $http.put(`is2/rest/${tipos}/modificar`, parametros);
        },

        agregarDatos: function (parametros, tipos) {
            return $http.post(`is2/rest/${tipos}/insertar`, parametros);
        },
        eliminarDatos: function (parametros, nroId) {
            return $http.delete(`is2/rest/${parametros}/${nroId}`,);
        },

        listarRoles: function () {
            return $http.get(`is2/rest/roles/listar`);
        },

        listarProyectosVista: function () {
            return $http.get(`is2/rest/vista-proyecto/listar-proyectos`);
        },

        listarHistoriasVistas: function () {
            return $http.get(`is2/rest/vista-proyecto/listar-historias`);
        },

        listarProyectos: function () {
            return $http.get(`is2/rest/vista-proyecto/listar-proyecto-asignado`);
        },

        listarBacklog: function () {
            return $http.get(`is2/rest/vista-proyecto/listar-vista-backlog`);
        },
        listarSprintBacklog: function () {
            return $http.get(`is2/rest/vista-proyecto/listar-vista-sprint`);
        },
    };
}]);