app.controller('SprintCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        $scope.sprint;
        parametros = 'sprint';
        // var parametro = {};
        $scope.listarSprint = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.sprint = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                                counts: [],
                                total: $scope.sprint.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.sprint, params.orderBy()) : $scope.sprint;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }
        

        $scope.listarSprint();


        $scope.modificar = function (sprint) {
            $localStorage.sprint = sprint;
            $state.go('app.modificar-sprint');
        };

        $scope.agregar = function () {
            $state.go('app.agregar-sprint');
        };

        $scope.eliminar = function (sprint) {
            $localStorage.sprint = sprint;
            nroId = $localStorage.sprint.sprintId;
            console.log($localStorage.sprint.sprintId)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.info('Se elimino correctamente');
                        $scope.listarSprint();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        };

    }]);