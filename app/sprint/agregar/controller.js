app.controller('AgregarSprintCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        
        $scope.datos = {};
        $scope.datos.sprint_description;
        $scope.datos.fecha_inicio = new Date();
        $scope.datos.fecha_fin = new Date();

        $scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }


        $scope.insertar = function () {
            tipos = 'sprint';
            parametros = {
                "sprintDescription": $scope.datos.sprint_description,
                "fechaInicio":$scope.datos.fecha_inicio,
                "fechaFin": $scope.datos.fecha_fin,
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        $state.go('app.sprint');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }

        $scope.limpiar = function () {
            $scope.datos.decription = "";
            $scope.datos.fecha_inicio = "";
            $scope.datos.fecha_fin = "";
        }

        $scope.cancelar = function () {
            $state.go('app.sprint');
        };

    }]);