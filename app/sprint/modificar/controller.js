app.controller('ModificarSprintCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.sprint = $localStorage.sprint;
        console.log($scope.selected.sprint);
        $scope.selected.sprint.fechaInicio = new Date();
        $scope.selected.sprint.fechaFin = new Date();

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#modificarSprint').modal('show');
        }


        $scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }



        $scope.modificar = function () {
            tipos = 'sprint';
            var parametros = {
                "sprintId": $scope.selected.sprint.sprintId,
                "sprintDescription": $scope.selected.sprint.sprintDescription,
                "fechaInicio": $scope.selected.sprint.fechaInicio,
                "fechaFin": $scope.selected.sprint.fechaFin,
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $state.go('app.sprint');
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                        unBlockUI();
                    });



        }



    }]);