app.controller('AgregarRolCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        

        tipos = 'roles';
        $scope.datos = {};
        $scope.datos.rolDescripcion;

        $scope.insertar = function () {
            parametros = {
                "rolDescripcion": $scope.datos.rolDescripcion
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;

                        $state.go('app.listar-rol');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.usuario');
        };

        $scope.limpiar = function () {
            $scope.datos.rolDescripcion = "";
        };
    }]);