app.controller('ModificarRolesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarRol = $localStorage.modificarRol;
        console.log($scope.selected.modificarDatos)
        tipos = 'roles';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioUsuario').modal('show');
        }

        $scope.modificarUsuario = function () {

            var parametros = {
                "idRoles": $scope.selected.modificarRol.idRoles,
                "rolDescripcion": $scope.selected.modificarRol.rolDescripcion
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        console.log('puto')
                        /*$localStorage.socio = response.data.dato;
                        $localStorage.logueado = true;*/
                        //toastr.info('Datos Actualizados');
                        $state.go('app.listar-rol');
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.usuario');
        };
    }]);