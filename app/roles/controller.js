app.controller('RolesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        console.log('hola');

        $scope.rol;
        parametros = 'roles';
        // var parametro = {};

        $scope.listarRles = function () {

            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.rol = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                                counts: [],
                                total: $scope.rol.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.rol, params.orderBy()) : $scope.rol;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        }
        $scope.listarRles();

        $scope.modificar = function (rol) {
            $localStorage.modificarRol = rol;
            console.log(rol)
            $state.go('app.modificar-rol');
        };

        $scope.eliminar = function (rol) {
            $localStorage.eliminarDatos = rol;
            nroId = $localStorage.eliminarDatos.idRoles;
            console.log($localStorage.eliminarDatos.idRoles)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.info('Se elimino el usuario');
                        $scope.listarRles();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        };

        $scope.agregar = function () {
            $state.go('app.agregar-rol');
        };

    }]);