app.controller('UsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        console.log('hola');

        blockUI();
        $scope.usuario;
        parametros = 'usuarios';
        // var parametro = {};

        $scope.listarUsuario = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.usuario = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                                counts: [],
                                total: $scope.usuario.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.usuario, params.orderBy()) : $scope.usuario;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.listarUsuario();



        $scope.modificar = function (usuario) {
            $localStorage.modificarDatos = usuario;
            console.log(usuario)
            $state.go('app.modificar-usuario');
        };

        $scope.eliminar = function (usuario) {
            $localStorage.eliminarDatos = usuario;
            nroId = $localStorage.eliminarDatos.idUsuarios;
            console.log($localStorage.eliminarDatos.idUsuarios)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.info('Se elimino el usuario');
                        $scope.listarUsuario();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        };

        $scope.agregar = function () {
            $state.go('app.agregar-usuario');
        };

    }]);