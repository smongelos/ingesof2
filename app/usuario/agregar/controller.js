app.controller('AgregarUsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        console.log('hola');

        tipos = 'usuarios';
        $scope.datos = {};
        $scope.datos.nombre;
        $scope.datos.users;
        $scope.datos.mail;
        $scope.datos.pass;
        $scope.datos.idUsuarios;
        $scope.datos.telefono;

        $scope.insertar = function () {
            parametros = {
                "nombre": $scope.datos.nombre,
                "mail": $scope.datos.mail,
                "pass": $scope.datos.users,
                "users": $scope.datos.users,
                "telefono": $scope.datos.telefono
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;

                        $state.go('app.usuario');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.usuario');
        };

        $scope.limpiar = function () {
            $scope.datos.nombre = "";
            $scope.datos.users = "";
            $scope.datos.mail = "";
            $scope.datos.pass = "";
            $scope.datos.telefono = "";
        };
    }]);