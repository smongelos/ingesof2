app.controller('ModificarUsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        console.log('hola');
        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        console.log($scope.selected.modificarDatos)
        tipos = 'usuarios';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioUsuario').modal('show');
        }

        $scope.modificarUsuario = function () {

            var parametros = {
                "nombre": $scope.selected.modificarDatos.nombre,
                "users": $scope.selected.modificarDatos.users,
                "mail": $scope.selected.modificarDatos.mail,
                "pass": $scope.selected.modificarDatos.pass,
                "idUsuarios": $scope.selected.modificarDatos.idUsuarios,
                "telefono": $scope.selected.modificarDatos.telefono
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        /*$localStorage.socio = response.data.dato;
                        $localStorage.logueado = true;*/
                        //toastr.info('Datos Actualizados');
                        //location.href="app/usuario";
                        $state.go('app.usuario');
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.usuario');
        };
    }]);