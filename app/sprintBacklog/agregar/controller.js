app.controller('AgregarSprintBacklogCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        
        
          
        $scope.datos = {};
        $scope.datos.idBacklog;
        $scope.datos.sprintId ;
        $scope.datos.idEstados ;


        $scope.estados = function () {
            blockUI();
            parametros = 'estados'
            $scope.estado = [];
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.estado = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.backlog = function () {
            blockUI();
            parametros = 'backlog'
            $scope.estado = [];
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.backloglis = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.sprint = function () {
            blockUI();
            parametros = 'sprint'
            $scope.estado = [];
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.sprintlis = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.sprint();
        $scope.estados ();
        $scope.backlog ();

       

        $scope.insertar = function () {
            tipos = 'sprint-backlog';
            parametros = {
                "idEstados": $scope.datos.idEstados,
                "sprintId":$scope.datos.sprintId,
                "idBacklog": $scope.datos.idBacklog,
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        $state.go('app.sprint-backlog');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }

        $scope.limpiar = function () {
            $scope.datos.idBacklog = "";
            $scope.datos.sprintId = "";
            $scope.datos.idEstados = "";
        }

        $scope.cancelar = function () {
            $state.go('app.sprint-backlog');
        };

      

    }]);