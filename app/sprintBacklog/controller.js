app.controller('SprintBacklogCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        $scope.sprint;
        parametros = 'sprint';
        // var parametro = {};
        $scope.listarSprint = function () {
            blockUI();
            MainFactory.listarSprintBacklog()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.sprint = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                                counts: [],
                                total: $scope.sprint.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.sprint, params.orderBy()) : $scope.sprint;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.listarSprint();

        $scope.agregar = function () {
            $state.go('app.agregar-sprint-backlog');
        };

        $scope.modificar = function (sprint) {
            $localStorage.sprint = sprint;
            console.log(sprint)
            $state.go('app.modificar-sprint-backlog');
        };

        $scope.eliminar = function (sprint) {
            $localStorage.sprint = sprint;
            parametros = 'sprint-backlog';
            nroId = $localStorage.sprint.id_sprint_backlog;
            console.log($localStorage.sprint.id_sprint_backlog)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.info('Se elimino correctamente');
                        $scope.listarSprint();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });


        };
    }]);