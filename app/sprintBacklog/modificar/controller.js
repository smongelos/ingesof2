app.controller('ModificarSprintBacklogCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.selected = {};
        $scope.selected.sprint = $localStorage.sprint;
        console.log($scope.selected.sprint);


        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#modificarSB').modal('show');
        }

        $scope.estados = function () {
            blockUI();
            parametros = 'estados'
            $scope.estado = [];
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.estado = response.data.lista;

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.backlog = function () {
            blockUI();
            parametros = 'backlog'
            $scope.estado = [];
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.backloglis = response.data.lista;

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.sprint = function () {
            blockUI();
            parametros = 'sprint'
            $scope.estado = [];
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.sprintlis = response.data.lista;

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });

        }

        $scope.sprint();
        $scope.estados();
        $scope.backlog();



        $scope.modificar = function () {
            tipos = 'sprint-backlog';
            parametros = {
                "idSprintBacklog":$scope.selected.sprint.id_sprint_backlog,
                "idEstados": $scope.selected.sprint.idEstados,
                "sprintId": $scope.selected.sprint.sprintId,
                "idBacklog": $scope.selected.sprint.idBacklog,
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $state.go('app.sprint-backlog');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        }



    }]);